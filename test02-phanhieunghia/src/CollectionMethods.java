import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet>planets){
		//Create a collection of planet into an ArrayList of planet
		Collection<Planet> innerP = new ArrayList<Planet>();
		//Loop through the input
		for(Planet p : planets ) {
			//If order is smaller or equal to 3, add the planet to the ArrayList
			if(p.getOrder() <= 3 ) {
				innerP.add(p);
			}
		}
	return innerP;
	}
}
