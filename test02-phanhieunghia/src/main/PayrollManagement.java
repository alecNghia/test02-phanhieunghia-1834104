package main;

import interfaces.Employee;
import utility.HourlyEmployee;
import utility.SalariedEmployee;
import utility.UnionizedHourlyEmployee;

public class PayrollManagement {
	//Where the program runs
	public static void main(String[] args) {
		
		//Employee array with the inputs
		Employee[] employees = new Employee[5];
		employees[0] = new HourlyEmployee(10, 14);
		employees[1] = new HourlyEmployee(40, 14);
		employees[2] = new SalariedEmployee(10000);
		employees[3] = new UnionizedHourlyEmployee(15, 13, 2000);
		employees[4] = new SalariedEmployee(8000);
		
		getTotalExpenses(employees);
	}
	//getTotalExpenses loop through the employees array and compute the total salary
	public static double getTotalExpenses(Employee[] employees) {
		double total = 0;
		for(int i = 0; i < employees.length; i++) {
			total = total + employees[i].getYearlyPay();
		}
		System.out.println(total);
		return total;
	}
}
