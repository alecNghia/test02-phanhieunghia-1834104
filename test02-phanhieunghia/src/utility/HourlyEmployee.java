package utility;

import interfaces.Employee;

public class HourlyEmployee implements Employee{
	//private fields 
	private double hoursWorked;
	private double hourlyPay;
	
	//HourlyEmployee constructor with inputs
	public HourlyEmployee(double hoursWorked, double hourlyPay) {
		this.hoursWorked = hoursWorked;
		this.hourlyPay = hourlyPay;
	}
	//YearlyPay calculation
	public double getYearlyPay() {
		double yearlyPay = hoursWorked * hourlyPay * 52;
		return yearlyPay;
	}
}
