package utility;

import interfaces.Employee;

public class SalariedEmployee implements Employee{
	//private fields
	private double yearly;

	//SalariedEmployee constructor
	public SalariedEmployee(double yearly) {
		this.yearly = yearly; 
	}
	
	//return the field
	public double getYearlyPay() {
		return this.yearly;
	}
	
	public String toString() {
		return ("Yearly salary: " + this.yearly);
	}
}
