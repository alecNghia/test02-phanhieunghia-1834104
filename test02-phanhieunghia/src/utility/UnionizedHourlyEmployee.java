package utility;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private double pensionContribution;
	
	//UnionizedHourlyEmployee constructor takes inputs the hours worked, hours paid and the pension
	public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay, double pensionContribution) {
		super(hoursWorked, hourlyPay);
		this.pensionContribution = pensionContribution;
	}
	
	//return the yearly pay with the pension
	public double getYearlyPay() {
		double yearlyPay = super.getYearlyPay() + pensionContribution;
		return yearlyPay;
	}
}
